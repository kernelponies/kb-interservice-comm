import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
        name="kakbima_interservice_comm",
        version="0.0.1",
        author="Alex Waweru",
        author_email="wawerua@kakbima.com",
        description="A package that makes it easy to get objects from other services within the Kakbima platform.",
        long_description=long_description,
        url="https://github.com/arocketman/git-and-pip",
        packages=setuptools.find_packages(),
        classifiers=[
                "Programming Language :: Python :: 3",
                "License :: OSI Approved :: MIT License",
                "Operating System :: OS Independent",
                ],
        )
