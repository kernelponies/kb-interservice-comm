import json
import os

import requests


class Client:

    @staticmethod
    def post(query, headers=None):
        url = os.environ.get("KAKBIMA_GATEWAY_URL")
        response = requests.post(url=url, json={'query': query}, headers=headers)
        status_code = response.status_code
        if status_code == 200:
            return {
                    "status_code": status_code,
                    "data": json.loads(response.text)
                    }
        else:
            return {
                    "status_code": status_code,
                    "data": None
                    }
