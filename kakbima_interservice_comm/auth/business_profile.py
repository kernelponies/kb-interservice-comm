from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class BusinessProfile:

    def __init__(self, uid: str, data: Dict = None) -> None:
        self.business_name = None
        self.business_description = None
        self.business_country = None
        self.physical_address = None
        self.postal_address = None
        self.business_phone_number1 = None
        self.business_phone_number2 = None
        self.business_phone_number3 = None
        self.business_email1 = None
        self.business_email2 = None
        self.business_email3 = None
        self.business_principal_officer = None
        self.business_type = None
        self.hq_physical_address = None
        self.branch_network = None
        self.licensed_entities = None
        self.website_url = None

        if not data:
            data = BusinessProfile.make_query(uid=uid)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str):
        query = """
        query{
          bussinessProfile(uid:"%s"){
            object{
              __typename
              ... on BusinessProfile{
                id
                businessName
                businessDescription
                businessCountry
                physicalAddress
                postalAddress
                businessPhoneNumber1
                businessPhoneNumber2
                businessPhoneNumber3
                businessEmail1
                businessEmail2
                businessEmail3
                businessPrincipalOfficer
                businessType
                hqPhysicalAddress
                branchNetwork
                licensedEntitiesType
                websiteUrl
              }
            }
          }
        }""" % uid
        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("bussinessProfile").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
