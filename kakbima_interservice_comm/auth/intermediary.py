from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class Intermediary:

    def __init__(self, uid: str, data: Dict = None) -> None:
        self.intermediary = None
        self.underwriter = None
        self.intermediary_type = None

        if not data:
            data = Intermediary.make_query(uid=uid)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str):
        query = """query{
                  intermediary(uid: %s){
                    object{
                      __typename
                      ... on Intermediary{
                        intermediary{
                          legalName
                          businessType
                          primaryEmail
                          primaryTelephone
                          country
                          countyProvinceState
                          cityTown
                        }
                        underwriter {
                          legalName
                          businessType
                          primaryEmail
                          primaryTelephone
                          country
                          countyProvinceState
                          cityTown
                        }
                        intermediaryType
                      }
                    }
                  }
                }""" % uid

        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("intermediary").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
