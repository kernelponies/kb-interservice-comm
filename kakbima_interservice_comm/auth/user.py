from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class User:

    def __init__(self, uid: str, data: Dict = None) -> None:
        self.first_name = None
        self.last_name = None
        self.account_email = None
        self.primary_phone_number = None
        self.username = None
        self.middle_name = None
        self.secondary_phone_number = None
        self.national_id = None
        self.passport_number = None
        self.recovery_email = None
        self.contact_email = None
        self.user_type = None
        self.user_permissions = None

        if not data:
            data = User.make_query(uid=uid)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str):
        query = """
        query {
          userAccount(uid: "%s") {
            object{
              __typename
              ... on User {
                firstName
                lastName
                accountEmail
                primaryPhoneNumber
                username 
                middleName 
                secondaryPhoneNumber 
                nationalId 
                passportNumber 
                recoveryEmail 
                contactEmail 
                userType 
                userPermissions
              }
            }
          }
        } """ % uid
        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("userAccount").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
