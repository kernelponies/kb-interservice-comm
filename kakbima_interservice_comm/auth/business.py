from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class Business:

    def __init__(self, uid: str, data: Dict = None) -> None:
        self.uid = uid
        self.legal_name = None
        self.business_type = None
        self.primary_email = None
        self.primary_telephone = None
        self.country = None
        self.county_province_state = None
        self.city_town = None

        if not data:
            data = Business.make_query(uid=uid)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str, info):
        query = """
        query{
          bussinessAccount(uid: "%s") {
            object{
              __typename
              ... on Business {
                legalName
                businessType
                primaryEmail
                primaryTelephone
                country
                countyProvinceState
                cityTown
              }
            }
          }
        }""" % uid

        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("bussinessAccount").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
