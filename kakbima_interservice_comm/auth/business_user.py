from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class BusinessUser:

    def __init__(self, uid: str, data: Dict, info) -> None:
        self.user = None
        self.business = None
        self.business_user_type = None
        self.is_admin = None

        if not data:
            data = BusinessUser.make_query(uid=uid, info=info)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str, info):
        query = """
        query{
          bussinessUserAccount(uid: "%s"){
            object{
              __typename
              ... on BusinessUser{
                business{
                  legalName
                  businessType
                  primaryEmail
                  primaryTelephone
                  country
                  countyProvinceState
                  cityTown
                }
                user {
                  id
                  firstName
                  lastName
                  accountEmail
                  userType
                }
                isAdmin
                businessUserType
              }
            }
          }
        }""" % uid

        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("bussinessUserAccount").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
