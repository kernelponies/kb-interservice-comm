from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class Customer:

    def __init__(self, uid: str, data: Dict, info) -> None:
        self.user = None
        self.business = None
        self.producer = None
        self.join_type = None
        self.customer_type = None

        if not data:
            data = Customer.make_query(uid=uid, info=info)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str, info):
        query = """
        query{
          customer(uid: "%s"){
            object{
              __typename
              ... on Customer{
                user{
                  id
                  firstName
                  lastName
                  accountEmail
                  userType
                  userPermissions
                }
                business{
                  legalName
                  businessType
                  primaryEmail
                  primaryTelephone
                  country
                  countyProvinceState
                  cityTown
                }
                producer{
                  legalName
                  businessType
                  primaryEmail
                  primaryTelephone
                  country
                  countyProvinceState
                  cityTown
                }
                joinType 
                customerType
              }
            }
          }
        }""" % uid

        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("customer").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
