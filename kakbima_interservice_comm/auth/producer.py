from typing import Dict

from kakbima_interservice_comm.client import Client
from kakbima_interservice_comm.utils import camel_case_to_snake_case


class Producer:

    def __init__(self, uid: str, data: Dict = None) -> None:
        self.individual_customer = None
        self.business_customer = None
        self.producer = None
        self.join_type = None
        self.customer_type = None

        if not data:
            data = Producer.make_query(uid=uid)

        for k, v in data.items():
            setattr(self, camel_case_to_snake_case(k), v)

    @staticmethod
    def make_query(uid: str):
        query = """
        query{
          producer (uid: "%s"){
            object{
              __typename
              ... on Producer{
                individualCustomer{
                  id
                  firstName
                  lastName
                  accountEmail
                  userType
                  userPermissions
                }
                businessCustomer{
                  legalName
                  businessType
                  primaryEmail
                  primaryTelephone
                  country
                  countyProvinceState
                  cityTown
                }
                producer{
                  legalName
                  businessType
                  primaryEmail
                  primaryTelephone
                  country
                  countyProvinceState
                  cityTown
                }
              }
            }
          }
        }""" % uid

        response = Client.post(query=query)
        if response.get("status_code"):
            return response.get("data").get("data").get("producer").get("object")
        else:
            raise Exception(
                    "Query failed to run by returning code of {}. {}".format(response.get("status_code"), query))
