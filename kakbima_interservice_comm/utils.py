def camel_case_to_snake_case(string: str):
    return ''.join(['_' + i.lower() if i.isupper() else i for i in string]).lstrip('_')
